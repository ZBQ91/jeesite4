/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.test.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.test.entity.TestDemo;
import com.jeesite.modules.test.service.TestDemoService;

/**
 * 测试demoController
 * @author zbq95
 * @version 2018-10-17
 */
@Controller
@RequestMapping(value = "${adminPath}/test/testDemo")
public class TestDemoController extends BaseController {

	@Autowired
	private TestDemoService testDemoService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TestDemo get(String id, boolean isNewRecord) {
		return testDemoService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("test:testDemo:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestDemo testDemo, Model model) {
		model.addAttribute("testDemo", testDemo);
		return "modules/test/testDemoList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("test:testDemo:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TestDemo> listData(TestDemo testDemo, HttpServletRequest request, HttpServletResponse response) {
		testDemo.setPage(new Page<>(request, response));
		Page<TestDemo> page = testDemoService.findPage(testDemo); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("test:testDemo:view")
	@RequestMapping(value = "form")
	public String form(TestDemo testDemo, Model model) {
		model.addAttribute("testDemo", testDemo);
		return "modules/test/testDemoForm";
	}

	/**
	 * 保存测试demo
	 */
	@RequiresPermissions("test:testDemo:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TestDemo testDemo) {
		testDemoService.save(testDemo);
		return renderResult(Global.TRUE, text("保存测试demo成功！"));
	}
	
	/**
	 * 删除测试demo
	 */
	@RequiresPermissions("test:testDemo:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TestDemo testDemo) {
		testDemoService.delete(testDemo);
		return renderResult(Global.TRUE, text("删除测试demo成功！"));
	}
	
}