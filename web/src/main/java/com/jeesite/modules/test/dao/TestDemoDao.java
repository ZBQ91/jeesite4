/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.test.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.test.entity.TestDemo;

/**
 * 测试demoDAO接口
 * @author zbq95
 * @version 2018-10-17
 */
@MyBatisDao
public interface TestDemoDao extends CrudDao<TestDemo> {
	
}