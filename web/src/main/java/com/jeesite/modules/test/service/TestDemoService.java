/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.test.service;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.test.entity.TestDemo;
import com.jeesite.modules.test.dao.TestDemoDao;

/**
 * 测试demoService
 * @author zbq95
 * @version 2018-10-17
 */
@Service
@Transactional(readOnly=true)
public class TestDemoService extends CrudService<TestDemoDao, TestDemo> {
	
	/**
	 * 获取单条数据
	 * @param testDemo
	 * @return
	 */
	@Override
	@Cacheable(value="cat")
	public TestDemo get(TestDemo testDemo) {
		System.out.println("不经过缓存1");
		return super.get(testDemo);
	}
	
	/**
	 * 查询分页数据
	 * @param testDemo 查询条件
	 * @param testDemo.page 分页对象
	 * @return
	 */
	@Override
	@Cacheable(value = "TestDemos")
	public Page<TestDemo> findPage(TestDemo testDemo) {
		System.out.println("不经过缓存2");
		return super.findPage(testDemo);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param testDemo
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(TestDemo testDemo) {
		super.save(testDemo);
	}
	
	/**
	 * 更新状态
	 * @param testDemo
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TestDemo testDemo) {
		super.updateStatus(testDemo);
	}
	
	/**
	 * 删除数据
	 * @param testDemo
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TestDemo testDemo) {
		super.delete(testDemo);
	}
	
}